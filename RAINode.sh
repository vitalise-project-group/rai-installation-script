#!/bin/bash
# Script for the installation of the RAI Node
# Dependencies:
# * registry.gitlab.com/vitalise-project-group
# * https://gitlab.com/vitalise-project-group/compolise
# Author: Cristina Molina Moreno

echo -e '\e[95;1m\nWellcome to Vitalise RAI node installation!!!\nMake sure to have access to Vitalise GitLab group.\n\e[0m'

set -o errtrace # Enable the err trap, code will get called when an error is detected

trap 'traperror $? $LINENO $BASH_LINENO "$BASH_COMMAND" $(printf "::%s" ${FUNCNAME[@]})'  ERR

traperror () {
    local err=$1 # error status
    local line=$2 # LINENO
    local linecallfunc=$3
    local command="$4"
    local funcstack="$5"
    echo -e '\e[91;1m<---\e[0m'
    echo -e '\e[91;1mERROR: line '$line' - command '$command' exited with status: '$err'\e[0m'
    if [ "$funcstack" != "::" ]; then
        echo -en '\e[91;1m   ... Error at '${funcstack}' \e[0m'
        if [ "$linecallfunc" != "" ]; then
            echo -en '\e[91;1mcalled at line '$linecallfunc'\e[0m'
        fi
    else
        echo -en '\e[91;1m   ... internal debug info from function '${FUNCNAME}' (line '$linecallfunc')\e[0m'
    fi
    echo -e '\e[91;1m\n--->\e[0m'
    exit
}

echo -e '\e[95;1m\n1. Checking software requirements...\n\e[0m'

installation(){
    sudo apt-get update -qq && sudo apt-get install -qq $1
    echo -e '\e[92;1m'$1' installed\n\e[0m'
}

debInst() {
    dpkg-query -Wf'${db:Status-abbrev}' "$1" 2>/dev/null | grep -q '^i'
}

for package in ca-certificates curl gnupg lsb-release
do
    if ! debInst $package; then
        echo -e '\e[93;1mInstalling '$package'...\n\e[0m'
        installation $package
    fi
done

if git --version; then
    echo -e '\e[93;1mGit already installed\n\e[0m'
else
    echo -e '\e[93;1mInstalling git...\n\e[0m'
    installation "git-all"
fi

if docker --version; then
    echo -e '\e[93;1mDocker already installed\n\e[0m'
else
    echo -e '\e[93;1mInstalling Docker...\n\e[0m'
    curl -fsSL https://get.docker.com -o get-docker.sh
    sh get-docker.sh
fi

if docker compose version; then
    echo -e '\e[93;1mDocker compose already installed\n\e[0m'
else
    echo -e '\e[93;1mInstalling docker compose...\n\e[0m'
    installation "docker-compose-plugin"
fi

echo -e '\e[95;1m\n2. Download rai_node_deploy repository...\n\e[0m'

SCRIPTPATH="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"
cd $SCRIPTPATH

if  [ ! -d "$SCRIPTPATH/compolise" ]
then
    git clone --branch v1_fixes https://gitlab.com/vitalise-project-group/compolise
    echo -e '\e[92;1mDownloaded version v1_fixes of Vitalise\e[0m'
else
    echo -e '\e[91;1mRAI node deploy repository is already downloaded from GitLab or the containers are still up. If you want to launch the installation from the beginning, please remove Docker containers and the existing ./compolise directory.\n\e[0m'
fi

echo -e '\e[95;1m\n3. Login to Docker GitLab registry...\n\e[0m'

docker login registry.gitlab.com/vitalise-project-group

echo -e '\e[95;1m\n4. Configuring environment variables...\n\e[0m'

cd $SCRIPTPATH/compolise

docker_compose(){
    for network in vitalise_monitoring vitalise_network vitalise_frontend
    do
        docker network create $network || true
    done
    for compose in docker-compose-monitoring.prod.yaml docker-compose-services.prod.yaml docker-compose-swag.prod.yaml
    do
        docker compose -f $compose up -d --quiet-pull
    done
}

change_URL(){
    filename1=".env.swag"
    filename2=".env.glokta"
    filename3=".env.grafana"
    echo -en '\e[1mInsert your RAI node URL with format vitalise-node.example.com: \e[0m'
    read -p '' URL
    sed -i '/^URL/d' $filename1
    sed -i '/^RAI_NODE_ROOT_URL/d' $filename2
    sed -i '/^GF_SERVER_ROOT_URL/d' $filename3
    sed -i '/^GF_SERVER_DOMAIN/d' $filename3
    echo -en "\nURL=$URL" >> $filename1
    echo -en '\nRAI_NODE_ROOT_URL=https://'$URL'' >> $filename2
    echo -en '\nGF_SERVER_ROOT_URL=https://'$URL'/' >> $filename3
    echo -en '\nGF_SERVER_DOMAIN=https://'$URL'/grafana' >> $filename3
    
    while true; do
        echo -en '\e[1mNew URL: \e[0m'$URL'\n'
        echo -en '\e[1mDo you want to use this URL?: \e[0m'
        read -p "" yn
        case $yn in
            [Yy]* ) docker_compose; break;;
            [Nn]* ) change_URL; break;;
            * ) echo -e '\e[91;1mPlease answer yes or no.\n\e[0m';;
        esac
    done
}

change_URL

rai_node_uuid(){
    while true; do
        echo -en '\e[1mDo you have a RAI node UUID? If not a random UUID will be asigned for your RAI node: \e[0m'
        read -p '' yn
        case $yn in
            [Yy]* ) echo -en '\e[1mInsert your RAI node UUID: \e[0m'; read -p '' UUID; break;;
            [Nn]* ) UUID=$(uuidgen); break;;
            * ) echo -e '\e[91;1mPlease answer yes or no.\n\e[0m';;
        esac
    done
    
    for filename in .env.bayaz .env.glokta .env.synth
    do
        sed -i '/^RAI_NODE_ID/d' $filename
        echo -en "\nRAI_NODE_ID=$UUID" >> $filename
    done
}

rai_node_uuid
docker_compose

echo -e '\e[95;1m\nCongratulations!! The installation is complete.\nYou can change environment variables at /compolise/.env files.\n\e[0m'
